#pragma once
#include "common.hpp"

namespace led {
namespace enums {

enum status {
#include "def/status.h"
, status_count
};

enum state {
#include "def/state.h"
, state_count
};

enum color {
#include "def/color.h"
, color_count
};
}//end enums
}//end led