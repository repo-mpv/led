#include <string>
#include "enum.h"

#ifdef ENUM_DEF
#undef ENUM_DEF
#endif
#define ENUM_DEF(value, desc) { (desc), (value) }

namespace led{
namespace enums{
state to_state(const std::string& value) {
    static const std::unordered_map<std::string, state> values = {
        #include "def/state.h"
    };
    const state bydefault = state(-1);
    return in_to_out(value, values, state(-1));
}

color to_color(const std::string& value) {
    static const std::unordered_map<std::string, color> values = {
        #include "def/color.h"
    };
    return in_to_out(value, values, color(-1));
}

status to_status(const std::string& value) {
    static const std::unordered_map<std::string, status> values = {
        #include "def/status.h"
    };
    return in_to_out(value, values, status(-1));
}
}//end enums
}//end led