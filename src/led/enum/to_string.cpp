#include <string>
#include "enum.h"

#ifdef ENUM_DEF
#undef ENUM_DEF
#endif
#define ENUM_DEF(value, desc) { (value), (desc) }

namespace led{
namespace enums{
static const std::string unknown = "unknown";
std::string to_string(state  value) {
    static const std::unordered_map<int, std::string> values  = { 
        #include "def/state.h"
    };
    return in_to_out((int)value, values, unknown);
}
std::string to_string(color  value) { 
    static const std::unordered_map<int, std::string> values  = { 
        #include "def/color.h"
    };
    return in_to_out((int)value, values, unknown);
}
std::string to_string(status value) { 
    static const std::unordered_map<int, std::string> values = {
        #include "def/status.h"
    };
    return in_to_out((int)value, values, unknown);
}
}//end enums
}//end led