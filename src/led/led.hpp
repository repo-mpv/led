#pragma once
#include "enum/enum.h"
#include <cstring>
#include <memory>
#include <mutex>
#include <sstream>
#include <vector>


namespace led {

namespace utils {
static bool equal(double a1, double a2, double eps = 1E-5) {
    return std::abs(a1-a2) < eps;
}
static bool equal_or_lesser(double a1, double a2, double eps = 1E-5) {
    if (equal(a1, a2, eps)) return true;
    return a1 < a2;
}
static bool equal_or_greater(double a1, double a2, double eps = 1E-5) {
    if (equal(a1, a2, eps)) return true;
    return a1 > a2;
}
}
namespace enums {
extern std::string to_string(state value);
extern std::string to_string(color value);
extern std::string to_string(status value);

extern state  to_state (const std::string& value);
extern color  to_color (const std::string& value);
extern status to_status(const std::string& value);
}
static std::string value_to_string(enums::state value)  { return enums::to_string(value); }
static std::string value_to_string(enums::status value) { return enums::to_string(value); }
static std::string value_to_string(enums::color value)  { return enums::to_string(value); }
static std::string value_to_string(double value)        { return std::to_string(value);   }

static std::string value_to_enum(enums::state value)  { return enums::to_string(value); }
static std::string value_to_enum(enums::status value) { return enums::to_string(value); }
static std::string value_to_enum(enums::color value)  { return enums::to_string(value); }

template<class T>
class param {
public:
    typedef T value_type;
private:
    T _value;
    std::mutex _mutex;
public:
    param() { _value = value_type(0); }
    value_type get() const { std::lock_guard<std::mutex> l_g(_mutex); return _value; }
    void set(value_type value) { std::lock_guard<std::mutex> l_g(_mutex); _value = value; }
    std::string to_string() { return value_to_string(_value); }
};
using state = param<enums::state>;
using state_ptr = std::shared_ptr<state>;
using color = param<enums::color>;
using color_ptr = std::shared_ptr<color>;
using rate = param<double>;
using rate_ptr = std::shared_ptr<rate>;


namespace command{
template<class param_t>
using value_check_cb = std::function<bool (param_t)>;
template <class param_t>
std::string get(param_t param, value_check_cb<param_t> cb) {
    if (cb && !cb(param)) {
        return enums::to_string(enums::failed);
    }
    return enums::to_string(enums::ok) + " " + param->to_string();
}

template <class value_type>
using args2value_cb = std::function<bool(const std::string&, value_type& )>;


template<class param_t, class value_type>
std::string set(param_t param, const std::string& args, args2value_cb<value_type> cb) {
    value_type value = value_type(0);
    if (cb && !cb(args, value)) {
        return enums::to_string(enums::failed);
    }
    param->set(value);
    return enums::to_string(enums::ok);
}
}

class controller {
    state_ptr _state;
    color_ptr _color;
    rate_ptr _rate;

    typedef std::function<std::string(const std::string&)> cmd_cb;
    std::unordered_map<std::string, cmd_cb> m_handlers;
public:
    controller() {
        init_params();
        registry();
    }

    std::string request(const std::string& request) {
        std::vector<std::string> tokens;
        if (!parse(request, tokens))
            return enums::to_string(enums::failed);
        return sub_request(tokens[0], tokens.size() == 2 ? tokens[1] : "");

    }
protected:
    void init_params() {
        _state = std::make_shared<state>();
        _color = std::make_shared<color>();
        _rate = std::make_shared<rate>();
    }
    void registry() {
        m_handlers = {
            { "get-led-state", 
                [=](const std::string& args) -> std::string { 
                    return command::get<state_ptr>(_state, nullptr); 
                }
            },
            { "set-led-state",
                [=](const std::string& args) -> std::string {
                    return command::set<state_ptr, state::value_type>(_state, args, 
                        [=](const std::string& args, state::value_type& value) -> bool {
                            return enums::args_to_enum<state::value_type>(args, value, enums::to_state);
                        }); 
                }
            },

            { "get-led-color", 
                [=](const std::string& args) -> std::string { 
                    return command::get<color_ptr>(_color, nullptr); 
                }
            },
            { "set-led-color",
                [=](const std::string& args) -> std::string {
                    return command::set<color_ptr, color::value_type>(_color, args, 
                        [=](const std::string& args, color::value_type& value) -> bool {
                            return enums::args_to_enum<color::value_type>(args, value, enums::to_color);
                        }); 
                }
            },

            { "get-led-rate", 
                [=](const std::string& args) -> std::string { 
                    return command::get<rate_ptr>(_rate, nullptr); 
                }
            },
            { "set-led-rate",
                [=](const std::string& args) -> std::string {
                    return command::set<rate_ptr, rate::value_type>(_rate, args, 
                        [=](const std::string& args, rate::value_type& value) -> bool {
                            value = std::stod(args);
                            return utils::equal_or_greater(value, 0.) && utils::equal_or_lesser(value, 5.);
                        }); 
                }
            }

        };
    }
    bool parse(const std::string& request, std::vector<std::string>& tokens) {
        if (request.empty())
            return false;
        std::istringstream stream(request);
        std::string token;
        while (std::getline(stream, token, ' ')) {
            tokens.push_back(token);
        }
        // COMMADN [ARG]
        return tokens.size() < 3;
    }
    std::string sub_request(const std::string& command, const std::string& args) {
        auto it = m_handlers.find(command);
        if (it == m_handlers.end()) return enums::to_string(enums::failed);
        return it->second(args);
    }
};
}
