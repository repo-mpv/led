#include "command.h"
#include "led/led.hpp"
#include <iostream>
#include <unistd.h>

namespace led{
namespace cli{
namespace enums{
extern std::string to_string(param value);
extern std::string to_string(command value);
extern std::string to_string(mode value);

extern param   to_param(const std::string& value);
extern command to_command(const std::string& value);
extern mode    to_mode(const std::string& value);
}//end enums
}//end cli
static std::string value_to_string(cli::enums::param value)    { return cli::enums::to_string(value); }
static std::string value_to_string(cli::enums::command value)  { return cli::enums::to_string(value); }
static std::string value_to_string(cli::enums::mode value)     { return cli::enums::to_string(value); }
}//end led


namespace led{
namespace cli {
static const char * unknown_command_str = "Unknown command.";
using get_args_cb = std::function<std::string()>;
bool specify_arg(const std::string& info, std::string& out) {
    //std::cin.clear();
    std::string input;
    std::cout << info;
    std::cin >> input;
    out = input;
    return std::cin.eof() != true;
}



template<typename T>
T enum_to_enum(const std::string& input, led::enums::str_to_enum_cb<T> cb) {
    if (isnumber(input)) {
        return (T)(std::stoi(input) - 1);
    }
    return cb(input);
}

template <typename T>
std::string enum_to_string(const std::string& input) {
    if (isnumber(input)) return led::value_to_string( (T)(std::stoi(input) - 1) );
    return input;
}

std::string align_name(const char *keyname, int align = 12) {
    std::string name; name.resize(align);
    std::memset(&name[0], ' ', align);
    auto length = strlen(keyname);
    if (length > align) length = align;
    std::memcpy(&name[0], keyname, length);
    return name;
}

template <typename T>
std::string get_values(int count) {
    std::string values = "[";
    for(int i = 0; i < count; ++i) {
        values += led::value_to_string((T)i);
        values += ":";
        values += std::to_string(i+1);
        values += "|";
    }
    values.pop_back();
    values += "]: ";
    //@note: values = "[...|some_name:i|...]: ";
    return values;
}

static std::string get_specify(const char *key, int align = 12) {
    return std::move(std::string("Specify ").append(align_name(key, align)));
}

template <typename T>
std::string get_specify_enum(const char* name, int count, int align = 12) {
    return get_specify(name, align).append(get_values<T>(count));
}

template <typename T>
bool specify_enum_to_enum(const char* name, int count, led::enums::str_to_enum_cb<T> cb, T& out, int align = 12) {
    std::string input;
    if (!specify_arg(get_specify_enum<T>(name, count, align), input))
        return false;
    out = enum_to_enum(input, cb);
    return true;
}

template <typename T>
std::string specify_enum_to_string(const char* name, int count, int align = 12) {
    std::string input;
    if (!specify_arg(get_specify_enum<T>(name, count, align), input))
        return "Input invalid.";
    return enum_to_string<T>(input);
}


static const std::unordered_map<int, get_args_cb> specify_cbs = {
        { enums::state, []() -> std::string { return specify_enum_to_string<led::enums::state>("state", led::enums::state_count); } },
        { enums::color, []() -> std::string { return specify_enum_to_string<led::enums::color>("color", led::enums::color_count); } },
        { enums::rate,  []() -> std::string { std::string out; return specify_arg(get_specify("rate").append("[0..5]: "), out) ? out : "invalid rate"; } }
    };

template<typename T>
std::string generate_arg(int count) {
    T current = (T)(rand() % count);
    return led::value_to_string(current);
}
static const std::unordered_map<int, get_args_cb> generate_cbs = {
        { enums::state, []() -> std::string { return generate_arg<led::enums::state>(led::enums::state_count); } },
        { enums::color, []() -> std::string { return generate_arg<led::enums::color>(led::enums::color_count); } },
        { enums::rate,  []() -> std::string { return generate_arg<double>(5); } }
    };

std::string command::build_name(enums::mode mode, enums::command cmd, enums::param param) {
    return std::string(led::value_to_string(cmd)).append("-led-").append(led::value_to_string(param));
}

std::string command::build_args(enums::mode mode, enums::param param) {
    const std::unordered_map<int, get_args_cb> &args_cbs = (mode == enums::mode::interactive) ? specify_cbs : generate_cbs;
    auto it = args_cbs.find(param);
    if (it == args_cbs.end()) return unknown_command_str;
    return it->second();
}

std::string command::build(enums::mode mode, enums::command cmd, enums::param param) {
    auto name = build_name(mode, cmd, param);
    if (cmd == enums::command::get) return name;
    return name.append(" ").append(build_args(mode, param));
}

bool command::interactive(std::string& out) {
    std::string input;
    enums::param param = (enums::param)-1;
    bool ret = true;
    while (param < 0 || param >= enums::param_count) {
        try {
            if (!specify_enum_to_enum<enums::param>("param", enums::param_count, enums::to_param, param))
                return false;
        } catch ( std::exception& e ) {
            std::cerr <<"[CLIENT][ERROR]: "<< e.what() << std::endl;
            param = (enums::param)-1;
        }
    }
    enums::command cmd = (enums::command)-1;
    input = "";
    while (cmd < 0 || cmd >= enums::cmd_count) {
        try {
            if (!specify_enum_to_enum<enums::command>("command", enums::cmd_count, enums::to_command, cmd))
                return false;
        } catch ( std::exception& e ) {
            std::cerr <<"[CLIENT][ERROR]: "<< e.what() << std::endl;
            cmd = (enums::command)-1;
        }
    }
    out = build(enums::mode::interactive, cmd, param);
    return ret;
}

bool command::test(std::string& out) {
    sleep(1);
    int cmd = rand() % enums::cmd_count;
    int param = rand() % (enums::param_count);
    enums::command cmd_t = (enums::command)cmd;
    enums::param param_t = (enums::param)param;
    std::cout << cmd_t << " param: " << param_t << std::endl;
    out = build(enums::mode::test, cmd_t, param_t);
    return true;
}
bool command::get(enums::mode mode, std::string& out) {
    switch (mode)
    {
    case enums::test: return command::test(out);
    default: return command::interactive(out);
    }
}

}//end cli
}//end led