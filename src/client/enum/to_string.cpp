#include <string>
#include "enum.h"

#ifdef ENUM_DEF
#undef ENUM_DEF
#endif
#define ENUM_DEF(value, desc) { (value), (desc) }

namespace led{
namespace cli{
namespace enums{
std::string to_string(param value) {
    static const std::unordered_map<int, std::string> values = {
        #include "def/param.h"
    };
    const std::string invalid = "invalid_command";
    return in_to_out((int)value, values, invalid);
}
std::string to_string(command value) {
    static const std::unordered_map<int, std::string> values = {
        #include "def/command.h"
    };
    const std::string invalid = "invalid_command";
    return in_to_out((int)value, values, invalid);
}
std::string to_string(mode value) {
    static const std::unordered_map<int, std::string> values = {
        #include "def/mode.h"
    };
    const std::string invalid = "invalid_mode";
    return in_to_out((int)value, values, invalid);
}
}//end enums
}//end cli
}//end led