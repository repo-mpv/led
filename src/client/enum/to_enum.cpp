#include <string>
#include "enum.h"

#ifdef ENUM_DEF
#undef ENUM_DEF
#endif
#define ENUM_DEF(value, desc) { (desc), (value) }

namespace led{
namespace cli{
namespace enums{
param to_param(const std::string& value) {
    static const std::unordered_map<std::string, param> values = {
        #include "def/param.h"
    };
    return in_to_out(value, values, param(-1));
}

command to_command(const std::string& value) {
    static const std::unordered_map<std::string, command> values = {
        #include "def/command.h"
    };
    return in_to_out(value, values, command(-1));
}

mode to_mode(const std::string& value) {
    static const std::unordered_map<std::string, mode> values = {
        #include "def/mode.h"
    };
    return in_to_out(value, values, mode(-1));
}
}//end enums
}//end cli
}//end led