#pragma once
#include "common.hpp"
namespace led {
namespace cli{
namespace enums {
enum param {
    #include "def/param.h"
    ,param_count
};
enum command {
    #include "def/command.h"
    ,cmd_count
};
enum mode {
    #include "def/mode.h"
    ,mode_count
};
}//end enums
}//end cli
}//end led