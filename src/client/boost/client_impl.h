#pragma once
#include <string>
#include <memory>
#include <boost/asio.hpp> 

class client_impl {
    boost::asio::io_service _io_service;
    std::shared_ptr<boost::asio::ip::tcp::socket> _socket;
public:
    client_impl();
    ~client_impl();
public:
    void connect(const std::string& host, unsigned port);
    void close();
    void send(const char *msg);
    std::string recv();
};

using client_impl_ptr = std::unique_ptr<client_impl>;