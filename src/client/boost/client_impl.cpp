#include "client_impl.h"
#include <iostream>
#include "helper.hpp"

client_impl::client_impl() {
    _socket = std::make_shared<boost::asio::ip::tcp::socket>(_io_service);
}

client_impl::~client_impl() {
    _socket->close();
}

void client_impl::connect(const std::string& host, unsigned port) {
    boost::asio::ip::tcp::resolver::query resolver_query(host, std::to_string(port), boost::asio::ip::tcp::resolver::query::numeric_service);
    boost::asio::ip::tcp::resolver resolver(_io_service);
    boost::system::error_code ec = boost::asio::error::host_not_found;
    boost::asio::ip::tcp::resolver::iterator it = resolver.resolve(resolver_query, ec);
    boost::asio::ip::tcp::resolver::iterator end;
    while (it != end) {
        _socket->connect(*it++, ec);
    }
}
void client_impl::close() {
    _socket->close();
}
void client_impl::send(const char *msg) {
    send_data(*(_socket.get()), msg);
}
std::string client_impl::recv() {
    return get_data(*(_socket.get()));
}