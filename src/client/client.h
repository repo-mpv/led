#pragma once
#include "client_impl.h"
namespace led {
class client {
    client_impl_ptr _pimpl;
public:
    client();
    void start(const std::string& host, unsigned port, int mode = 0);
};
}//end led