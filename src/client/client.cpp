#include "client.h"
#include <iostream>
#include "command.h"
namespace led {
client::client()
    : _pimpl(std::make_unique<client_impl>()) { 
        srand (time(NULL));
    }

void client::start(const std::string& host, unsigned port, int mode) {
    std::cout << "[CLIENT][INFO]: Connect to " << host << ":" << port << std::endl;
    std::string addr = std::string("tcp://").append(host).append(":").append(std::to_string(port));
    _pimpl->connect(host, port);
    while(true) {
        std::string request;
        if (!cli::command::get((cli::enums::mode)mode, request)) {
            std::cout << "[CLIENT][INFO]: stop" << std::endl;
            break;
        }
        _pimpl->send (request.c_str());
        std::cout << "[CLIENT][INFO][REQUEST]: "<<request <<std::endl;
        std::string reply = _pimpl->recv();
        std::cout << "[CLIENT][INFO][REPLY]: "<<reply <<std::endl;
    }
    _pimpl->close();
}
}//end led