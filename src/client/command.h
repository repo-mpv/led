#pragma once
#include <memory>
#include <string>
#include "enum/enum.h"
namespace led {
namespace cli{
class command;
using command_ptr = std::shared_ptr<command>;
class command {
public:
    static std::string build(enums::mode mode, enums::command cmd, enums::param param);
    static std::string build_name(enums::mode mode, enums::command cmd, enums::param param);
    static std::string build_args(enums::mode mode, enums::param param);
public:
    static bool interactive(std::string& out);
    static bool test(std::string& out);
    static command_ptr create(enums::command cmd, enums::param param);
    static bool get(enums::mode mode, std::string& out);
};
}//end cli
}//end led