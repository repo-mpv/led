#pragma once
#include <string>
#include <memory>

class client_impl {
    void *_context;
    void *_socket;
    int _last_error;
public:
    client_impl();
    ~client_impl();
public:
    void connect(const std::string& host, unsigned port);
    void close();
    void send(const char *msg);
    std::string recv();
};

using client_impl_ptr = std::unique_ptr<client_impl>;