#include "client_impl.h"
#include <iostream>
#include "helper.hpp"

client_impl::client_impl() {
    _context = nullptr;
    _socket = nullptr;
    _last_error = 0;
}

client_impl::~client_impl() {
    close();
}

void client_impl::connect(const std::string& host, unsigned port) {
    if (_last_error != 0) throw std::runtime_error(std::string("[ZMQ]: err: ").append(std::to_string(_last_error)));
    close();
    _context = zmq_ctx_new();
    _socket = zmq_socket (_context, ZMQ_REQ);
    std::string addr = std::string("tcp://").append(host).append(":").append(std::to_string(port));
    zmq_connect (_socket, addr.c_str());
}
void client_impl::close() {
    if (_socket) {
        _last_error = zmq_close (_socket);
        if (_last_error != 0) std::cerr << "[CLIENT][ZMQ][ERROR]: zmq_close: " << _last_error <<std::endl;
        else _socket = nullptr;
    }
    if (_context) { 
        _last_error = zmq_ctx_destroy (_context);
        if (_last_error != 0) std::cerr << "[CLIENT][ZMQ][ERROR]: zmq_ctx_destroy: " << _last_error <<std::endl;
        else _context = nullptr;
    }
}
void client_impl::send(const char *msg) {
    s_send(_socket, msg);
}
std::string client_impl::recv() {
    return s_recv(_socket);
}