#include <iostream>
#include <getopt.h>
#include "client.h"
#include "utils/common.hpp"

static const char *short_options = "h:p:vt:";
static struct option long_options[] = {
    { "host", required_argument, NULL, 'h' },
    { "port", required_argument, NULL, 'p' },
    { "type", required_argument, NULL, 't' },
    { "version", no_argument, NULL, 'v' },
    { NULL, 0, NULL, 0 }
};

int main(int argc, char* argv[])
{
    try {
        std::string host = "127.0.0.1";
        unsigned port = 5555;
        int opt = 0;
        int mode = 0;
        do {
            opt = getopt_long(argc, argv, short_options, long_options, NULL);
            switch (opt) {
                case 'h': host = optarg; break;
                case 'p': port = std::stoi(optarg); break;
                case 't': mode = std::stoi(optarg); break;
                case 'v': return print_version();
            }
        } while (opt >= 0);
        led::client cli;
        cli.start(host, port, mode);
    } catch (std::exception& e) {
        std::cerr << "[CLIENT][ERROR]: " << e.what() << std::endl;
    }
    return 0; 
}