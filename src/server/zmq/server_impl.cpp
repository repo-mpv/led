#include "server_impl.h"
#include <exception>
#include <iostream>
#include <thread>
#include "zmq.h"
#include "helper.hpp"



static void worker(unsigned port, led::controller& led) {
    void *context = zmq_ctx_new ();

    //  Socket to talk to clients
    void *responder = zmq_socket (context, ZMQ_REP);
    std::string addr = std::string("tcp://localhost:").append(std::to_string(port));
    zmq_connect (responder, addr.c_str());

    while (1) {
        //  Wait for next request from client
        char *string = s_recv (responder);
        std::string request(string);
        free (string);
        std::cout << "[SERVER][INFO][REQUEST]: " << request << std::endl;
        auto reply = led.request(request);
        std::cout << "[SERVER][INFO][REPLY]: " << reply << std::endl;
        s_send (responder, reply.c_str());
    }
    //  We never get here, but clean up anyhow
    zmq_close (responder);
    zmq_ctx_destroy (context);
}


server_impl::server_impl() { }

 void server_impl::stop() {
     throw std::runtime_error("server::stop: TODO: implemented");
 }

void server_impl::start(unsigned port) {
    std::thread work(std::bind(worker, port+1, _led)); work.detach();
    void *context = zmq_ctx_new ();
    void *frontend = zmq_socket (context, ZMQ_ROUTER);
    void *backend  = zmq_socket (context, ZMQ_DEALER);
    std::string addr = std::string("tcp://*:").append(std::to_string(port));
    zmq_bind (frontend, addr.c_str());
    addr = std::string("tcp://*:").append(std::to_string(port + 1));
    zmq_bind (backend,  addr.c_str());

    //  Initialize poll set
    zmq_pollitem_t items [] = {
        { frontend, 0, ZMQ_POLLIN, 0 },
        { backend,  0, ZMQ_POLLIN, 0 }
    };
    //  Switch messages between sockets
    while (1) {
        zmq_msg_t message;
        zmq_poll (items, 2, -1);
        if (items [0].revents & ZMQ_POLLIN) {
            while (1) {
                //  Process all parts of the message
                zmq_msg_init (&message);
                zmq_msg_recv (&message, frontend, 0);
                int more = zmq_msg_more (&message);
                zmq_msg_send (&message, backend, more? ZMQ_SNDMORE: 0);
                zmq_msg_close (&message);
                if (!more)
                    break;      //  Last message part
            }
        }
        if (items [1].revents & ZMQ_POLLIN) {
            while (1) {
                //  Process all parts of the message
                zmq_msg_init (&message);
                zmq_msg_recv (&message, backend, 0);
                int more = zmq_msg_more (&message);
                zmq_msg_send (&message, frontend, more? ZMQ_SNDMORE: 0);
                zmq_msg_close (&message);
                if (!more)
                    break;      //  Last message part
            }
        }
    }
    //  We never get here, but clean up anyhow
    zmq_close (frontend);
    zmq_close (backend);
    zmq_ctx_destroy (context);
}