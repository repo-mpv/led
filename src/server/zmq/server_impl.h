#pragma once
#include <memory>

#include "led/led.hpp"

class server_impl {
    led::controller _led;
public:
    server_impl();
    void start(unsigned port);
    void stop();
};
using server_impl_ptr = std::unique_ptr<server_impl>;