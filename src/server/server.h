#pragma once
#include "server_impl.h"

class server {
    server_impl_ptr _pimpl;
public:
    server();
    void start(unsigned port);
    void stop();
};