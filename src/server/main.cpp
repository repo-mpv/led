
#include <ctime>
#include <iostream>
#include <string>
#include <thread>
#include <getopt.h>
#include "server.h"
#include "utils/common.hpp"
#include <signal.h>

static const char *short_options = "p:v";
static struct option long_options[] = {
    { "port", required_argument, NULL, 'p' },
    { "version", no_argument, NULL, 'v' },
    { NULL, 0, NULL, 0 }
};

server g_srv;
static void signal_handler(int sig)
{
    printf("SIGINT\n");
    g_srv.stop();
}

int main(int argc, char *argv[])
{
  try
  {
    signal(SIGINT, signal_handler);
    unsigned port = 5555;
        int opt = 0;
        do {
            opt = getopt_long(argc, argv, short_options, long_options, NULL);
            switch (opt) {
                case 'p': port = std::stoi(optarg); break;
                case 'v': return print_version();
            }
        } while (opt >= 0);
    g_srv.start(port);
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}