#include "server.h"
#include <iostream>
server::server()
  : _pimpl(std::make_unique<server_impl>()) { }

void server::start(unsigned port) {
  std::cout << "[SERVER][INFO]: start"<< std::endl;
  std::cout << "[SERVER][INFO]: port " << port << std::endl;
  _pimpl->start(port);
}

void server::stop() {
  std::cout << "[SERVER][INFO]: stop"<< std::endl;
  _pimpl->stop();
}