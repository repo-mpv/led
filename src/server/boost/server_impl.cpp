#include "server_impl.h"
#include "helper.hpp"

#include <iostream>
#include <exception>
#include <thread>

#include <boost/enable_shared_from_this.hpp>
using boost::asio::ip::tcp;

class session
  : public std::enable_shared_from_this<session>
{
public:
  using handler_cb = std::function<std::string(const std::string&)>;
  using handlers_type = std::unordered_map<std::string, handler_cb>;
public:
  session(boost::asio::ip::tcp::socket socket, led::controller& led);
  void start() {
    do_read();
  }
  void close() {
    do_close();
  }
private:
  void do_read();
  void do_write(std::size_t length);
  void do_payload(std::size_t length);
  void do_close();
  void registry();
  std::string get_reply(const std::string& request);
private:
  boost::asio::ip::tcp::socket _socket;
  enum { max_length = 1024 };
  char _data[max_length];
  int _offset = 0;
  std::string _reply;
  led::controller& _led;
  handlers_type _handlers;
};


server_impl::server_impl() { }

void server_impl::accept() {
  if (!_acceptor) return;
  _acceptor->async_accept(
        [this](boost::system::error_code ec, tcp::socket socket)
        {
          if (!ec)
          { 
            std::make_shared<session>(std::move(socket), _led)->start();
          }
          accept();
        });
}

void server_impl::start(unsigned port) {
    boost::asio::io_context io_context;
    boost::asio::signal_set signals(io_context, SIGINT, SIGTERM);
    signals.async_wait([&](auto, auto) {
      io_context.stop();
      std::cout << "\n[SERVER][INFO]: Server was stoped."<< std::endl;
    });
    _acceptor = create_acceptor(io_context, port);
    accept();
    io_context.run();
}

void server_impl::stop() {
}

acceptor_ptr server_impl::create_acceptor(boost::asio::io_context& io_context, short port) {
  return std::make_shared<boost::asio::ip::tcp::acceptor>(io_context, tcp::endpoint(tcp::v4(), port));
}

session::session(boost::asio::ip::tcp::socket socket, led::controller& led)
    : _socket(std::move(socket))
    , _led(led)
{
  registry();
}
void session::do_read()
{
  auto self(shared_from_this());
  _socket.async_read_some(boost::asio::buffer(_data + _offset, max_length - _offset),
      [this, self](boost::system::error_code ec, std::size_t length)
      {
        if (!ec)
        {
          do_payload(length);
        } else do_close();
      });
}

void session::do_payload(std::size_t length)
{
  std::istringstream stream(_data, length);
  std::string request;
  auto prev = stream.tellg();
  _reply.clear();
  while (std::getline(stream, request, '\n')) {
    if (stream.tellg() > 0) {
      std::string reply = get_reply(request);
      reply.push_back('\n');
      _reply.append(reply);
      std::cout << "[SERVER][INFO][REQUEST]: " << request << std::endl;
      std::cout << "[SERVER][INFO][REPLY]: " << reply << std::endl;
      prev = stream.tellg();
    } else {
      _offset += length - prev;
      std::memcpy(_data, _data + prev, _offset);
      std::memset(_data + _offset, 0, sizeof(_data) - _offset);
    }
  }
  if (prev == length) { 
    _offset = 0;
    std::memset(_data, 0, sizeof(_data));
  }
  do_write(_reply.size());
}

void session::do_write(std::size_t length)
{
  auto self(shared_from_this());
  boost::asio::async_write(_socket, boost::asio::buffer(_reply.c_str(), length),
      [this, self](boost::system::error_code ec, std::size_t /*length*/)
      {
        if (ec) {
          do_close();
        }else do_read();
      });
}
void session::do_close() {
  _socket.close();
}

void session::registry() {
  _handlers = {
    { "led", [=](const std::string& request) { return _led.request(request); } }
  };
}

std::string session::get_reply(const std::string& request) {
  std::string command;
  if (request.find("led") != std::string::npos)
    command = "led";
  auto it = _handlers.find(command);
  if (it == _handlers.end())
    return "Unknown command.";
  return std::move(it->second(request));
}