#pragma once
#include <memory>
#include "led/led.hpp"
#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
using acceptor_ptr = std::shared_ptr<boost::asio::ip::tcp::acceptor>;
class server_impl {
    led::controller _led;
    acceptor_ptr _acceptor;
public:
    server_impl();
    void start(unsigned port);
    void stop();
    void accept();
public:
    static acceptor_ptr create_acceptor(boost::asio::io_context& io_context, short port);
};
using server_impl_ptr = std::unique_ptr<server_impl>;