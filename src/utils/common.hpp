#pragma once
#include "version.h"
#include <iostream>
inline int print_version() {
    std::cout << "VERSION_DEV: " << VERSION_DEV << std::endl;
    std::cout << "BUILD_TIME: " << BUILD_TIME << std::endl;
    return 0;
}
#include <algorithm>
#include <string>
#include <cctype>
inline bool isnumber(const std::string& input) {
    int count = std::count_if(input.begin(), input.end(), 
                         [](unsigned char c){ return std::isdigit(c); }
                        );
    return count == input.size();
}

#define ENUM_DEF(value, desc) value
#include <unordered_map>
#include <functional>
namespace led{
template <typename TIn, typename TOut>
TOut in_to_out(const TIn& value, const std::unordered_map<TIn, TOut>& values, const TOut& bydefault) {
    auto it = values.find(value);
    if (it == values.end()) return bydefault;
    return it->second;
}

namespace enums {
template<typename T>
using str_to_enum_cb = std::function<T(const std::string&)>;

template<typename T>
bool args_to_enum(const std::string& args, T& value, str_to_enum_cb<T> cb) {
    auto v = cb ? cb(args) : (T)-1;
    value = v;
    return value != (T)-1;
}
}
}