#pragma once
#include <boost/asio.hpp> 
#include <boost/asio/read_until.hpp>

static std::string get_data(boost::asio::ip::tcp::socket& socket) 
{ 
    boost::asio::streambuf buf; 
    read_until(socket, buf, "\n");
    std::istream is(&buf);
    std::string data;
    std::getline(is, data);
    return std::move(data); 
} 
  
static void send_data(boost::asio::ip::tcp::socket& socket, const std::string& message) 
{ 
    boost::asio::write(socket, boost::asio::buffer(message + "\n"));
}