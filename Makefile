DEBUG   ?= 0
OBJ_DIR := ./_out/obj
OUT_DIR := ./_out/bin
OBJ_EXT := o
SRC_DIR := ./src
SRC_IMPL ?= boost

TARGET_SRV  := ledsrv
TARGET_CLI  := ledcli
TARGET_LED  := led

INCLUDES := -I$(SRC_DIR)
INCLUDES += -I$(SRC_DIR)/server/
INCLUDES += -I$(SRC_DIR)/server/$(SRC_IMPL)
INCLUDES += -I$(SRC_DIR)/client/
INCLUDES += -I$(SRC_DIR)/client/$(SRC_IMPL)
INCLUDES += -I$(SRC_DIR)/utils/
INCLUDES += -I$(SRC_DIR)/utils/$(SRC_IMPL)
CPPFLAGS := -std=c++14 -g
ifeq ($(DEBUG), 1)
	CPPFLAGS += -O0
else
	CPPFLAGS += -O3
endif
LDLIBS   := -lstdc++ -lpthread
ifeq ($(SRC_IMPL), boost)
	LDLIBS += -lboost_system
else ifeq ($(SRC_IMPL), zmq)
	LDLIBS += -lzmq
endif

CXXFLAGS = $(INCLUDES)

SRC_SRV  := $(addprefix $(SRC_DIR)/server/, server.cpp main.cpp)
SRC_SRV_IMPL  := $(addprefix $(SRC_DIR)/server/$(SRC_IMPL)/, server_impl.cpp)
SRC_CLI  := $(addprefix $(SRC_DIR)/client/, client.cpp command.cpp main.cpp)
SRC_CLI_ENUM  := $(addprefix $(SRC_DIR)/client/enum/, to_enum.cpp to_string.cpp)
SRC_CLI_IMPL  := $(addprefix $(SRC_DIR)/client/$(SRC_IMPL)/, client_impl.cpp)


SRC_LED  := $(addprefix $(SRC_DIR)/led/enum/, to_enum.cpp to_string.cpp)

OBJ_SRV := $(addprefix $(OBJ_DIR)/server/, $(notdir $(patsubst %.cpp, %.$(OBJ_EXT), $(SRC_SRV))))
OBJ_SRV += $(addprefix $(OBJ_DIR)/server/, $(notdir $(patsubst %.cpp, %.$(OBJ_EXT), $(SRC_SRV_IMPL))))
OBJ_CLI := $(addprefix $(OBJ_DIR)/client/, $(notdir $(patsubst %.cpp, %.$(OBJ_EXT), $(SRC_CLI))))
OBJ_CLI += $(addprefix $(OBJ_DIR)/client/, $(notdir $(patsubst %.cpp, %.$(OBJ_EXT), $(SRC_CLI_IMPL))))
OBJ_CLI += $(addprefix $(OBJ_DIR)/client/, $(notdir $(patsubst %.cpp, %.$(OBJ_EXT), $(SRC_CLI_ENUM))))
OBJ_LED := $(addprefix $(OBJ_DIR)/led/, $(notdir $(patsubst %.cpp, %.$(OBJ_EXT), $(SRC_LED))))

$(OBJ_DIR)/server/%.o: $(SRC_DIR)/server/%.cpp
	@mkdir -p $(OBJ_DIR)/server
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $^ -o $@

$(OBJ_DIR)/server/%.o: $(SRC_DIR)/server/$(SRC_IMPL)/%.cpp
	@mkdir -p $(OBJ_DIR)/server
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $^ -o $@

$(OBJ_DIR)/client/%.o: $(SRC_DIR)/client/%.cpp
	@mkdir -p $(OBJ_DIR)/client
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $^ -o $@

$(OBJ_DIR)/client/%.o: $(SRC_DIR)/client/$(SRC_IMPL)/%.cpp
	@mkdir -p $(OBJ_DIR)/client
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $^ -o $@

$(OBJ_DIR)/client/%.o: $(SRC_DIR)/client/enum/%.cpp
	@mkdir -p $(OBJ_DIR)/client
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $^ -o $@

$(OBJ_DIR)/led/%.o: $(SRC_DIR)/led/enum/%.cpp
	@mkdir -p $(OBJ_DIR)/led
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -c $^ -o $@

$(OUT_DIR)/$(TARGET_SRV): $(OBJ_SRV) $(OBJ_LED)
	@mkdir -p $(OUT_DIR)
	$(CXX) $(LDFLAGS) $^ -o  $(OUT_DIR)/$(TARGET_SRV) $(LDLIBS)

$(OUT_DIR)/$(TARGET_CLI): $(OBJ_CLI) $(OBJ_LED)
	@mkdir -p $(OUT_DIR)
	$(CXX) $(LDFLAGS) $^ -o  $(OUT_DIR)/$(TARGET_CLI) $(LDLIBS)

VERSION_FILE        := $(SRC_DIR)/version.h
BUILD_TIME          := $(shell date -R)
GIT_TAG_COMMITS_SHA := $(shell git describe)
version:
	@echo '#pragma once' > $(VERSION_FILE)
	@echo '#define VERSION_DEV "$(GIT_TAG_COMMITS_SHA)"' >> $(VERSION_FILE)
	@echo '#define BUILD_TIME "$(BUILD_TIME)"' >> $(VERSION_FILE)

all: srv cli
cli: version $(OUT_DIR)/$(TARGET_CLI)
srv: version $(OUT_DIR)/$(TARGET_SRV)
clean:
	@rm -f -R $(OBJ_DIR)
	@rm -f -R $(OUT_DIR)

image: clean isandbox
ibuild:
	docker-compose -f docker-compose.build.yml up --force-recreate led_build
isandbox: ibuild
	docker-compose build srv cli
up:
	docker-compose up --force-recreate
prune:
	docker rmi $(docker images -f "dangling=true" -q) --force