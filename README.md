**BUILD**

***dependences*** : _boost_ (by default) | _zmq_

make all **OR** make SRC_IMPL=zmq all

**START**

./ledsrv [<port>]

./ledcli [-p <port>] [-h <ip>]

**DEMO**

***SERVER***

[SERVER][INFO]: start

[SERVER][INFO]: port 5555

[SERVER][INFO][REQUEST]: get-led-state

[SERVER][INFO][REPLY]: OK on

[SERVER][INFO][REQUEST]: set-led-color yellow

[SERVER][INFO][REPLY]: FAILED

***CLIENT***

[CLIENT][INFO]: Connect to 127.0.0.1:5555

Specify param   [state:1|color:2|rate:3]: 1

Specify command [get:1|set:2]: 1

[CLIENT][INFO][RESPONSE]: OK on

Specify param       [state:1|color:2|rate:3]: 2

Specify command     [get:1|set:2]: 2

Specify color       [red:1|green:2|blue:3]: yellow

[CLIENT][INFO][RESPONSE]: FAILED
